import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

servico = Service(ChromeDriverManager().install())
navegador = webdriver.Chrome(service=servico)

""" Código para Microsoft Edge
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service

servico = Service(EdgeChromiumDriverManager().install())
navegador = webdriver.Edge(service=servico)
"""

navegador.get('https://www.python.org/')
navegador.find_element('xpath', '//*[@id="documentation"]/a').click()

time.sleep(5)

navegador.find_element('xpath', '//*[@id="id-search-field"]').send_keys('classes')

time.sleep(5)

navegador.find_element('xpath', '//*[@id="submit"]').click()

time.sleep(5)
